import unittest
from mysoundsin import SoundSin


class TestSoundSin(unittest.TestCase):

    def test_creation(self):
        sound = SoundSin(1, 440, 1)
        self.assertEqual(len(sound.buffer), 44100)

    def test_frequency(self):
        sound = SoundSin(1, 440, 1)
        expected = 440
        self.assertEqual(sound.frequency, expected)

    def test_duration(self):
        sound = SoundSin(2, 440, 1)
        expected = 2
        self.assertEqual(sound.duration, expected)


if __name__ == '__main__':
    unittest.main()
