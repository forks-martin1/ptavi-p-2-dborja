import unittest
from mysound import Sound
import math


class TestSin(unittest.TestCase):

    def test_sin(self):
        sound = Sound(1)

        freq = 440
        amp = 10000

        sound.sin(freq, amp)

        # Check number of samples
        self.assertEqual(44100, len(sound.buffer))

        # Check sample values
        expected = amp * math.sin(2 * math.pi * freq * 0)
        self.assertEqual(expected, sound.buffer[0])

        expected = amp * math.sin(2 * math.pi * freq * 0.1)
        self.assertAlmostEqual(expected, sound.buffer[4410], delta=1)

        expected = amp * math.sin(2 * math.pi * freq * 0.25)
        self.assertAlmostEqual(expected, sound.buffer[11025], delta=1)


if __name__ == '__main__':
    unittest.main()
